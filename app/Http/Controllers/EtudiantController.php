<?php

namespace App\Http\Controllers;

use App\Etudiant;
use Illuminate\Http\Request;
use App\Http\Requests\EtudiantStoreRequest;
class EtudiantController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etudiants = Etudiant::all();
        return view('home', compact('etudiants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view : back
     */
    public function add(EtudiantStoreRequest $request)
    {
        //
        Etudiant::create($request->all());

        #return to home page
        return back()->with('status', trans('etudiant.msgenregistrementok'));
    }

    /**
     * Affiche les informations de l'étudiant
     *
     * @param  int $id: id de l'étudiant
     * @return view : show
     */
    public function show($id)
    {
        //
        $etudiant = Etudiant::findorfail($id);
        return view('etudiant.show', compact('etudiant'));
    }

    /**
     * Afficher le formulaire pour la modification
     *
     * @param  int $id : id de l'étudiant
     * @return view : edit
     */
    public function edit($id)
    {
        //
        $etudiant = Etudiant::findorfail($id);
        return view('etudiant.edit', compact('etudiant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id : id de l'étudiant
     * @param  Request $request : requête update
     * @return view : sur la vue précédante
     */
    public function update(Request $request, $id)
    {
        //
        $etudiant = Etudiant::findorfail($id); // si l'id n'existe pas il retourne une erreur 404
        $etudiant->nom = $request->input('nom');
        $etudiant->prenom = $request->input('prenom');
        $etudiant->save();
        
        return back()->with('status', trans('etudiant.msgmiseajourok'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Etudiant  $etudiant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Etudiant::destroy($id);
        return back()->with('status', trans('etudiant.msgdestroyok'));
    }
}
