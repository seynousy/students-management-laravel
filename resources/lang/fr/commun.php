<?php

return [
  'enregistrer' => 'Enregistrer ',
  'detail' => 'Détails',
  'modifier' => 'Modifier',
  'delete' => 'Supprimer',
  'action' => 'Actions',
  'titredetails' => 'Détails de l\'étudiant',
  'msgenregistrementok' => 'Étudiant ajouté avec succès',
  'msgmiseajourok' => 'Étudiant modifié avec succès',
  'titremodification' => 'Mis à jour des informations de l\'étudiant',
  'retour' => "Précédant"
];