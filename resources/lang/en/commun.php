<?php

return [
  'enregistrer' => 'Save ',
  'detail' => 'Details',
  'modifier' => 'Edit',
  'delete' => 'Delete',
  'action' => 'Actions',
  'titredetails' => 'Student Details',
  'msgenregistrementok' => 'Student successfully added',
  'msgmiseajourok' => 'Student successsfully updated',
  'titremodification' => 'Student informations update',
  'retour' => "Back"
];