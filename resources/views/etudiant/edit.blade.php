@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-offset-md-2">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h1>{{ trans('commun.titremodification') }}</h1>
        </div>
        <div class="panel-body">
          @if(session('status'))
            <div class="alert alert-success">
              {{session('status')}}
            </div>
          @endif
          @include('etudiant._form')
          <a href="{{ route('home') }}">{{ trans('commun.retour')}} </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection