<table class="table">
  <tr>
    <th>
      {{ trans('etudiant.nom') }}
    </th>
    <th>
      {{ trans('etudiant.prenom') }}
    </th>
    <th>
      {{ trans('commun.action') }}
    </th>
  </tr>
  @foreach($etudiants as $etudiant)
  <tr>
    <td> {{ $etudiant->nom }} </td>
    <td> {{ $etudiant->prenom }} </td>
    <td> <a href="{{ route('showEtudiant', $etudiant->id) }}">{{ trans('commun.detail')}} </a></td>
    <td> <a href="{{ route('editEtudiant', $etudiant->id) }}">{{ trans('commun.modifier')}} </a></td>
    <td> <a href="{{ route('deleteEtudiant', $etudiant->id) }}">{{ trans('commun.delete')}} </a></td>
  </tr>
  @endforeach
</table>