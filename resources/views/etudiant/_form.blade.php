@if(isset($etudiant))
{!! Form::model($etudiant, ['route' => ['updateEtudiant', $etudiant->id], 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'addEtudiant']) !!}
@endif

{!! Form::label("{{ trans('etudiant.nom') }}", trans('etudiant.nom')) !!}
{!! Form::text('nom') !!}
{!! Form::label("{{ trans('etudiant.prenom') }}", trans('etudiant.prenom')) !!}
{!! Form::text('prenom') !!}
<button type="submit" class="btn btn-sm btn-primary m-t-n-xs">{{ trans('commun.enregistrer') }}</button>

{!! Form::close() !!}

<br>
<div class="row">
  <div class="col-md-12">
    @if ($errors->any())
      <div class="alert alter-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
</div>