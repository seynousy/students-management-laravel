@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-offset-md-2">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h1>{{ trans('commun.titredetails') }}</h1>
        </div>
        <div class="panel-body">
          <br>
          <br>
          <h3>{{ trans('etudiant.nom') }}:    <b>{{ $etudiant->nom }}</b></h3>
          <h3>{{ trans('etudiant.prenom') }} :    <b>{{ $etudiant->prenom }}</b></h3>
          <a href="{{ route('home') }}">{{ trans('commun.retour')}} </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection